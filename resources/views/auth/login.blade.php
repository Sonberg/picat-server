@extends('app') @section('content')

<div class="container">
    <div class="columns large-6 large-offset-3 medium-8 medium-offset-2 small-12">
       <img src="{{ asset('assets/img/picat-logo.png') }}" class="picat-logo" alt="">
        <div class="callout large-12 small-12">
            <h5>Driver du en nattklubb eller event?</h5>
            <p>Bli en del av Picat</p>
            <a class="button rounded" href="register" style="z-index:10000;">Registrera dig</a>
        </div>
        <form method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div>
                <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required>
                {{ $errors->first('email') }}
            </div>

            <div>
                <input type="password" name="password" id="password" placeholder="Password" required>
                {{ $errors->first('password') }}
            </div>

            <div>
                <div style="float: left" class="hide">
                    <input type="checkbox" checked="checked" name="remember"> Remember Me </div>

                <div style="float: right">
                    <button class="button large rounded" type="submit" href="#">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
