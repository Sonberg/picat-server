@extends('app') @section('content')
<div class="container">
    <div class="columns large-6 large-offset-3 medium-8 medium-offset-2 small-12">
       <img src="{{ asset('assets/img/picat-logo.png') }}" class="picat-logo" alt="">
        <form method="POST" data-abide novalidate>
            {!! csrf_field() !!}

            <div>
                <input type="text" name="name" value="{{ old('name') }}" required placeholder="Company">
            </div>

            <div>
                <input type="email" name="email" value="{{ old('email') }}" required placeholder="Email">
            </div>

            <div>
                <input type="password" name="password" required placeholder="Password">
            </div>

            <div>
                <input type="password" name="password_confirmation" required placeholder="Password confirmation">
            </div>

            <div style="float:left">
                <a href="login" class="button rounded" style="z-index: 1000;">Already member?</a>
            </div>

            <div style="float:right">
                <button class="button large" type="submit" href="#">Register</button>
            </div>
        </form>
    </div>
</div>
@endsection
