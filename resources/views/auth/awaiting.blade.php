@extends('app') @section('content')
<div class="container">
    <div class="columns large-6 large-offset-3 medium-8 medium-offset-2 small-12">
       <img src="{{ asset('assets/img/picat-logo.png') }}" class="picat-logo" alt="">
        <div class="callout large-12 small-12">
            <h5>Your account is awaiting activation</h5>
            <p>If you have questions, please let us know!</p>
        </div>
        <a href="/" class="button rounded large text-center" style="
        margin: 20px auto;
        display: table;
        ">
            <span class="fi-arrow-left"></span>
        </a>
    </div>
</div>
@endsection