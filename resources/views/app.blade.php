<!DOCTYPE html>
<html>
  <head>
      <title>Picat</title>
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/6.1.2/foundation.min.css">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/foundation/6.1.2/foundation.css">
      <link rel="stylesheet" href="/assets/css/foundation-datepicker.min.css">
      <link href="/assets/css/app.css" rel="stylesheet">
      <meta name="viewport" content="width=device-width">

      <link href="/assets/foundation-icons/foundation-icons.css" rel="stylesheet">
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.1.2/foundation.js"></script>-->

      <!-- Alertify -->
      <link rel="stylesheet" href="/assets/css/alertify.css">
      <link rel="stylesheet" href="/assets/css/default.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  </head>
<body>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

      <!-- Foundation -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/foundation/6.1.2/foundation.min.js"></script>

    <!-- Content -->
    @yield('content')

    <script>
        $(document).foundation();
    </script>

    <script src="/assets/js/alertify.min.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-75301574-1', 'auto');
      ga('send', 'pageview');

  </script>

    <!-- Flash Message -->
    @if(isset($flash) ) @if($flash[0] !== 0)
    <flash type="{{$flash[0]}}" class="hide">{{$flash[1]}}</flash>
    @endif @endif


</body>

</html>
