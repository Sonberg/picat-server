<div class="top-bar">
    <div class="top-bar-title">
        <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
            <span class="menu-icon dark" data-toggle></span>
        </span>
        <img src=" {{ asset('/assets/img/logo.png') }}" class="logo" alt="">
    </div>
    <div id="responsive-menu">
        <div class="top-bar-left">

        </div>
        <div class="top-bar-right">
            <ul class="menu">

                <!-- Drop-down -->
                <ul class="dropdown menu ul" data-dropdown-menu>
                    <li class="li"><a href="/dashboard" class="button {{ Ekko::isActiveURL('/dashboard') }}"><span class="fi-home"></span> Home</a></li>

                    @if($user->account_type === 'admin')
                    <li class="li"><a href="/dashboard/companies" class="button {{ Ekko::isActiveURL('/dashboard/companies') }}"><span class="fi-torsos-all"></span>  Customers</a></li>
                    <li class="li"><a href="/dashboard/timelimit" class="button {{ Ekko::isActiveURL('/dashboard/timelimit') }}"><span class="fi-clock"></span> Timelimit</a></li>
                    <li class="li"><a href="/dashboard/flag" class="button {{ Ekko::isActiveURL('/dashboard/flag') }}"><span class="fi-flag"></span> Flagged <span class="c-badge">{{count($flagged)}}</span></a></li>
                    <!-- <li class="li"><a href="/dashboard/countries" class="button {{ Ekko::isActiveURL('/dashboard/countries') }}"><span class="fi-flag"></span></a></li> -->
                    <li class="li"><a href="/dashboard/events" class="button {{ Ekko::isActiveURL('/dashboard/events') }}"><span class="fi-page-multiple"></span> All events</a></li>
                    @endif

                    @if(count($events) !== 0)
                        <li class="li">
                            <a href="#" class="button {{ Ekko::isActiveURL('/dashboard/event/') }}"><span class="fi-page-multiple"></span> Events</a>
                            <ul class="menu vertical">
                               @foreach ($events as $event)
                                <li class="event-link hide"><a href="/dashboard/event/{{$event->id}}">{{$event->title}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                    <li class="li"><a href="/dashboard/add" class="button {{ Ekko::isActiveURL('/dashboard/add') }}"><span class="fi-plus"></span> New event</a></li>

                    <li class="li">
                        <a href="/dashboard/settings" class="button {{ Ekko::isActiveURL('/dashboard/settings') }}"><span class="fi-wrench"></span> Settings</a>
                    </li>
                </ul>

            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.event-link').removeClass('hide');
  });
</script>
