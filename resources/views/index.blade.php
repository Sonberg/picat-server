@extends('app') @section('content')
<div class="index">
    <div class="row large-12">
        <img src="assets/img/picat-logo.png" class="picat-logo" alt="">
        <a href="https://itunes.apple.com/se/app/picat/id1100823206?l=sv&ls=1&mt=8"><img src="assets/img/appstore.svg" class="float-center" style="margin-top: 20px;" alt=""></a>
    </div>
    <a href="/policy" class="policy-link"> Användaravtal / Terms of use</a>
</div>
<!-- <div class="index-content"></div> -->
<script src="/assets/js/parallax.js"></script>
@endsection
