@extends('app') @section('content')
@include('navbar')
<div class="view">
    <div class="row columns">
        <div class="row intro">
           <!-- Edit/Delete Button -->
            <div class="c-btn-group float-right">
                <a href="{{$event->id}}/edit" class="c-btn c-btn--secondary" style="outline: none !important;">Edit event</a>
                <a href="{{$event->id}}/delete" class="c-btn c-btn--secondary" style="outline: none !important;">Delete</a>
            </div>

            <div class="large-2 medium-6 small-6 main-thumb">
                    @if(isset($event->thumb))
                    <img src="{{$event->thumb}}" alt="" class="main-thumb"> @endif
                </div>
                <div class="columns large-10 row medium-6 small-6 float-right">

                    <h1>{{$event->title}}</h1>
                    <h6 class="subheader"> {{$event->date_start}} - {{$event->date_stop}} </h6>
                </div>

            <div class="columns large-12 medium-12 small-12 activity">
                @include('dashboard.activity')
            </div>

            <div class="column large-12 quick c-banner" style="border-radius: 1000px">
                <!-- Quick Settings -->
                <form action="/dashboard/event/{{$event->id}}/update/visibility" method="post">
                    {!! csrf_field() !!}
                    <fieldset class="large-6 columns text-center" id="event-state" state="{{$event->isVisible}}">
                        <legend>Visible in app:</legend>
                        <input type="radio" name="eventStatus" value="visible" id="eventVisible">
                        <label for="eventVisible">Visible</label>
                        <input type="radio" name="eventStatus" value="hidden" id="eventHidden">
                        <label for="eventHidden">Hidden</label>
                    </fieldset>
                </form>
                <form action="/dashboard/event/{{$event->id}}/update/new-posts" method="post">
                    {!! csrf_field() !!}
                    <fieldset class="large-6 columns text-center" id="postsIsVisible" state="{{$event->postsIsVisible}}">
                        <legend>New posts are: </legend>
                        <input type="radio" name="newPostStatus" value="visible" id="newPostStatusVisible">
                        <label for="newPostStatusVisible">Visible</label>
                        <input type="radio" name="newPostStatus" value="hidden" id="newPostStatusHidden">
                        <label for="newPostStatusHidden">Hidden</label>
                    </fieldset>
                </form>
            </div>
        </div>

        <!-- Recent posts -->
        <div class="columns large-12 medium-12 small-12 recent">
            <form action="/dashboard/posts/{{$event->id}}/edit" method="post">
                {!! csrf_field() !!}
                <div class="bar-placholder">
                    <div class="edit-bar c-banner">
                        <div class="large-6 columns">
                            <h4 style="">Recent posts <small>({{$postcount}})</small></h4>
                        </div>
                        @if($postcount !== 0)
                        <div class="edit-button large-6 columns">
                            <span class="label secondary" id="num-selected">0</span>
                            <input type="submit" class="button visible-post visability" name="show_button" value="">
                            <input type="submit" class="button invisible-post visability" name="hide_button" value="">
                            <input type="submit" class="button alert" value="Delete" name="delete_posts">
                        </div>
                        @endif
                    </div>
                </div>

                @if($postcount === 0)
                <h2>No posts to show!</h2> @else

                <div class="scroll small-up-2 medium-up-4 large-up-4" event="{{$event->id}}">

                </div>
                <div id="load">
                    <div class="loader">Loading...</div>
                </div>
                @endif
            </form>
        </div>
    </div>
</div>

<!-- Infinit Scroll -->
<script type="text/javascript" src="/assets/js/jquery.infinitescroll.js"></script>
<script>
    infinitScroll();
</script>

<!-- Edit Posts -->
<script type="text/javascript" src="/assets/js/edit-posts.js"></script>

<!-- Chart.js -->
<script type="text/javascript" src="/assets/chartjs/Chart.js"></script>
<script type="text/javascript" src="/assets/js/chart.js"></script>


<!-- Time Age -->
<script type="text/javascript" src="/assets/js/jquery.timeago.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("time.timeago").timeago();
    });
</script>


<!-- Google Maps -
<script>
    var tooltip = new Foundation.Tooltip($('label.switch-paddle'));
    var lat={{$event->latitude}},
        lng={{$event->longitude}};
</script>

<script type="text/javascript" src="{{ asset('/assets/js/maps.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyBX7cJkAz8yUv4eBnosGlPPkHaEGRSHmV8&callback=initMap"></script>
-->
@endsection
