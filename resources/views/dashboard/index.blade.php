@extends('app') @section('content')
@include('navbar')
<div class="row text-center">
    <div class="c-banner c-banner--unpinned">
        <h1>Welcome, {{$user->name}}</h1>
        <p>The dashboard is used to monitor your acticity for your events </p>
        @if(count($events) == 0)
            <button class="c-btn c-btn--primary">Add your first event</button>
        @endif
    </div>
    <div class="analys">
        <div class="large-4 columns"><span class="f-alpha">{{count($events)}}</span>
            <h3>Events</h3></div>
        <div class="large-4 columns"><span class="f-alpha">1</span>
            <h3>Posts</h3></div>
        <div class="large-4 columns"><span class="f-alpha">1</span>
            <h3>New posts last 24h</h3></div>
    </div>
</div>
@endsection
