@extends('app') @section('content') @include('navbar')
<div class="row" id="events">
  <div class="row" data-equalizer data-equalize-on="medium" id="">
    @foreach($eventsAll as $e )
    <div class="medium-4 columns">
      <div class="callout" data-equalizer-watch >
        <a href="/dashboard/event/{{$e->id}}">
          <div class="image-event @if($e->isVisible != 1) inactive @endif" style="background-image: url('{{$e->thumb}}')">
            <div class="overlay u-pad-m">
              <p>{{$e->title}}</p>
            </div>
          </div>
        </a>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection
