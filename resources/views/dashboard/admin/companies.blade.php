@extends('app') @section('content') @include('navbar')
<div class="admin">
    <div class="row columns" style="padding-top:40px">
        <div class="columns large-12" id="companies">
            <form method="post">
               {!! csrf_field() !!}
                <div class="bar-placholder">
                    <div class="edit-bar">
                        <div class="large-6 small-6 columns">
                            <h4 style="">All Companies <small>({{count($users)}})</small></h4>
                        </div>

                        <div class="edit-button large-6 small-6 columns">
                            <input type="submit" class="button float-right" value="Save changes" name="save">
                            <input type="text" class="float-right search" style="width: 40%;" placeholder="Search" />
                        </div>
                    </div>
                </div>
                <table class="hover stack c-table">
                    <thead>
                        <tr>
                            <th width="80">Active</th>
                            <th width="80">ID</th>
                            <th>Company name</th>
                            <th width="200"  class="hide-for-small-only">Phone Number</th>
                            <th width="200">Email</th>
                            <th width="150">Account type</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        @foreach($users as $u)
                        <tr>
                            <td class="active-check" state="{{$u->isActive}}">
                              @if($u->isActive == 1)
                                <input type="checkbox" name="{{$u->id}}" checked="checked">
                              @else
                                <input type="checkbox" name="{{$u->id}}">
                              @endif
                            </td>
                            <td class="id">{{$u->id}}</td>
                            <td class="name">{{$u->name}}</td>
                            <td class="number hide-for-small-only">{{$u->number}}</td>
                            <td class="email">{{$u->email}}</td>
                            <td class="type">{{$u->account_type}}</td>

                            <form method="post" action="/dashboard/company/delete" class="hide-for-small-only">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{$u->id}}" />
                                <td><button type="submit" class="button alert rounded hide-for-small-only"><span class="fi-x"></span></button></td>
                            </form>

                            @if($u->account_type !== 'admin')
                            <td>
                                <form method="post" action="/dashboard/company/promote" class="hide-for-small-only">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="id" value="{{$u->id}}" />
                                    <button type="submit" class="button rounded"><span class="fi-like"></span></button>
                                </form>
                            </td>
                            @else
                            <td>
                                <form method="post" action="/dashboard/company/promote" class="hide-for-small-only">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="id" value="{{$u->id}}" />
                                    <button type="submit" class="button rounded"><span class="fi-dislike"></span></button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<!-- List.js -->
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.2.0/list.min.js"></script>
<script>
    $(document).ready(function () {
        var options = {
            valueNames: ['name', 'id', 'number', 'email', 'type']
        };

        var companyList = new List('companies', options);
    });
</script>

<!-- Time Age -->
<script type="text/javascript" src="//timeago.yarp.com/jquery.timeago.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $("td.timeago").timeago();
    });
</script>

<!-- Edit Posts -->
<script type="text/javascript" src="/assets/js/edit-posts.js"></script>

@endsection
