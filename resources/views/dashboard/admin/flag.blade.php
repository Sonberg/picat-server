@extends('app') @section('content') @include('navbar')
<div class="row" id="flag">
  @if(count($flagged) == 0)
    <h1 class="text-center u-mar-vertical-l">No flagged posts right now</h1>
  @else
  <table class="c-table" style="margin-top: 50px;">
  <thead>
    <th>#</th>
    <th width="50"></th>
    <th></th>
    <th></th>
  </thead>
  <tbody>
    @foreach($flagged as $f)
    @if(isset($f["image"]))
    <tr>
      <td>{{$f["post"]}}</td>
      <td class="thumb-img"><img src="{{$f['image']}}" alt=""  class="thumb-img"/></td>
      <td class="small"><h5>{{$f["text"]}}</h5> <p style="margin-bottom: 0;">{{$f["created_at"]}}</p></td>
      <td>
        <p style="margin-bottom: 0; margin-top: 30px;"><b>Flagged by </b> {{$f["flag_by"]}}</p>
        <p><b>Post uploaded by</b> {{$f["post_by"]}}</p>
      </td>
      <td>
        <form class="c-btn-group float-right" method="post">
          {!! csrf_field() !!}
          <input type="hidden" name="id" value="{{$f['id']}}">
          <input type="hidden" name="type" value="{{$f['type']}}">
          <input type="hidden" name="post" value="{{$f['post']}}">
          <button type="submit" value="remove" name="action" class="c-btn c-btn--secondary"> <i class="fa fa-camera-retro"></i> Remove post</button>
          <button type="submit" value="block" name="action" class="c-btn c-btn--secondary"><i class="fa fa-ban" aria-hidden="true"></i> Block post owner</button>
          <button type="submit" value="check" name="action" class="c-btn c-btn--primary"><i class="fa fa-check" aria-hidden="true"></i> Remove flag</button>
        </form>
      </td>
    </tr>
    @endif
    @endforeach
  </tbody>
  </table>
  @endif
</div>
@endsection
