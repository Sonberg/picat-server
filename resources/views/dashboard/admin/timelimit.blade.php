@extends('app') @section('content') @include('navbar')
<div class="timelimit">
    <div class="row columns">
    <div class="c-banner large-12 columns margin">
    <h1 class="text-center" style="margin-top: 20px; margin-bottom: -25px;">Timelimited watermarks</h1>
      <form method="post" enctype="multipart/form-data" class="hide-for-small-only">
      {!! csrf_field() !!}
       <div class="large-6 columns">
           <input type="text" name="title" value="" class="large-6 float-center c-input" placeholder="Title">
        <input type="text" class="c-input" name="date_start" value="" placeholder="Start date" id="dpd1">
        <input type="text" class="c-input" value="" name="date_stop" placeholder="End date" id="dpd2">
       </div>
       <div class="large-6 columns">
            <input type="file" name="thumb" class="float-left" placeholder="" id="logotype" accept="image/png">
            <button type="submit" class="c-btn c-btn--primary float-right"><span class="fi-plus"></span></button>
       </div>
       </form>
       </div>
    <table class="hover stack c-table">
       @if(count($timelimit) !== 0)
        <thead>
            <tr>
                <th width="200">Thumb</th>
                <th>Title</th>
                <th width="100">Start date</th>
                <th width="100">End date</th>
                <th width="200" class="hide-for-small-only">Created at</th>
                <th></th>
            </tr>
        </thead>
        @endif
        <tbody class="list">
            @foreach($timelimit as $t)
            <tr>
                <td class="thumb"><img src="{{$t->url}}" alt="" class="thumb" /></td>
                <td class="title">{{$t->title}}</td>
                <td class="date_start">{{$t->date_start}}</td>
                <td class="date_stop">{{$t->date_stop}}</td>
                <td class="timeago hide-for-small-only" datetime="{{$t->created_at}}">{{$t->created_at}}</td>
                <form method="post" action="/dashboard/timelimit/delete" class="hide-for-small-only">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{$t->id}}" />
                    <input type="hidden" name="url" value="{{$t->url}}" />
                    <td><button type="submit" class="button alert rounded hide-for-small-only"><span class="fi-x"></span></button></td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>

<!-- Datepicker -->
<script src="/assets/js/foundation-datepicker.min.js"></script>
<script type="text/javascript" src="/assets/js/datepicker.js"></script>

<!-- Uploader -->
<script type="text/javascript" src="/assets/js/upload.js"></script>

@endsection
