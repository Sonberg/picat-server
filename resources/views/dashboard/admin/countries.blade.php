@extends('app') @section('content') @include('navbar')
<div class="timelimit">
    <div class="row columns">
        <h3 class="text-center" style="margin-top: 40px;">Managed country watermark</h3>
        <form method="post" enctype="multipart/form-data" class="hide-for-small-only">
            {!! csrf_field() !!}
            <div class="large-6 columns">
                <input type="text" name="title" value="" class="large-6 float-center" placeholder="Title">
                <input type="text" name="short" value="" class="large-2 float-center" placeholder="Short name">
            </div>
            <div class="large-6 columns">
                <input type="file" name="thumb" class="float-left" placeholder="" id="logotype" accept="image/png">
                <button type="submit" class="button float-right large rounded"><span class="fi-plus"></span></button>
            </div>
        </form>
        <table class="hover stack">
            @if(count($countries) !== 0)
            <thead>
                <tr>
                    <th width="40">Short name</th>
                    <th width="200">Thumb</th>
                    <th>Country</th>
                    <th width="200" class="hide-for-small-only">Created at</th>
                </tr>
            </thead>
            @endif
            <tbody class="list">
                @foreach($countries as $c)
                <tr>
                    <td class="short">{{$c->short_name}}</td>
                    <td class="thumb"><img src="{{$c->url}}" alt="" class="thumb" /></td>
                    <td class="title">{{$c->country}}</td>
                    <td class="timeago hide-for-small-only" datetime="{{$c->created_at}}">{{$c->created_at}}</td>
                    <form method="post" action="/dashboard/countries/delete" class="hide-for-small-only">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{{$c->id}}" />
                        <input type="hidden" name="url" value="{{$c->url}}" />
                        <td>
                            <button type="submit" class="button alert rounded hide-for-small-only"><span class="fi-x"></span></button>
                        </td>
                    </form>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- Uploader -->
<script type="text/javascript" src="/assets/js/upload.js"></script>

@endsection
