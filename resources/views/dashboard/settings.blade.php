@extends('app') @section('content')
@include('navbar')
<div class="row columns text-center" style="padding-top: 40px">
   <h1>Edit account settings</h1>
   <form class="" method="post">
     {!! csrf_field() !!}

     @if ($errors->has('name'))
      <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
    @endif
     <input type="text" value="{{$user->name}}" name="name" placeholder="Company Name" class="c-input {{ $errors->has('name') ? ' is-invalid' : '' }}">

     @if ($errors->has('email'))
      <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
     <input type="text" value="{{$user->email}}" name="email" placeholder="Email" class="c-input {{ $errors->has('email') ? ' is-invalid' : '' }}">

     @if ($errors->has('phone'))
      <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
    @endif
     <input type="text" value="{{$user->number}}" name="phone" placeholder="Phone number" class="c-input {{ $errors->has('phone') ? ' is-invalid' : '' }}">

     @if ($errors->has('new-password'))
      <span class="help-block"><strong>{{ $errors->first('new-password') }}</strong></span>
    @endif
     <input type="password" name="new-password" placeholder="New password" class="c-input {{ $errors->has('new-password') ? ' is-invalid' : '' }}">

     @if ($errors->has('new-password-repeat'))
      <span class="help-block"><strong>{{ $errors->first('new-password-repeat') }}</strong></span>
    @endif
     <input type="password" name="new-password-repeat" placeholder="Repeat new password" class="c-input {{ $errors->has('new-password-repeat') ? ' is-invalid' : '' }}">

     @if ($errors->has('old-password'))
      <span class="help-block"><strong>{{ $errors->first('old-password') }}</strong></span>
    @endif
     <input type="password" name="old-password" placeholder="Old password" class="c-input {{ $errors->has('old-password') ? ' is-invalid' : '' }}">

     <input type="submit" value="save" class="button large rounded" class="c-input is-invalid">
   </form>
    <hr>
    <a href="/logout" class="button alert large float-center" style="width: 200px; margin-top: 40px;">Logout</a>
</div>
@endsection
