@extends('app') @section('content')
@include('navbar')
<div class="edit">
    <div class="row">

        <div class="row">
            <div class="main-img">
                @if($event->thumb)
                <form action="image/delete" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{$event->thumb}}" name="url">
                    <input type="hidden" value="logotype" name="type">
                    <input type="submit" class="button alert" value="x">
                    <img src="{{$event->thumb}}" alt="" class="main-thumb float-center">
                </form> @else
                <form action="image/upload" method="post" class="upload-form" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="hidden" value="logotype" name="type">
                    <div class="no-image">
                        <input type="file" name="thumb" class="upload" accept="image/*">
                        <div class="title">Click to upload</div>
                    </div>
                </form>
                @endif
            </div>
            <hr>
            <h3 class="subheader text-center">Watermarks</h3>
            <div class="water-img large-5 float-center" data-equalizer data-equalize-on="medium">
                @foreach($watermarks as $watermark)
                <form class="column float-left large-6 equal" data-equalizer-watch action="image/delete" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" value="{{$watermark->url}}" name="url">
                    <input type="hidden" value="watermarks" name="type">
                    <input type="submit" class="button alert" value="x">
                    <img src="{{$watermark->url}}" class="" alt="">
                </form>
                @endforeach @for ($i=0; $i<(2-count($watermarks)); $i++) <form class="column float-left large-6 equal upload-form" action="image/upload" method="post" data-equalizer-watch enctype="multipart/form-data">
                    <div class="no-image">
                        {!! csrf_field() !!}
                        <input type="hidden" value="watermarks" name="type">
                        <input type="file" name="url" class="upload" accept="image/*">
                        <div class="title">Click to upload</div>
                    </div>
                    </form>
                    @endfor
            </div>
            <hr>
            <form action="/dashboard/event/{{$event->id}}/edit" method="post">
               {!! csrf_field() !!}
                <input type="text" name="title" placeholder="Title" value="{{$event->title}}" class="title">
                <input type="text" name="url" placeholder="Website" value="{{$event->url}}" class="">
                <script>
                    $(document).ready(function () {
                        $("#{{$event->event_type}}").prop("checked", true);
                    });
                </script>
                <div class="text-center">
                    <input type="radio" name="type" value="timelimit" id="timelimit" class="event-type">
                    <label for="timelimit"><span class="fi-clock"></span> Timelimited</label>
                    <input type="radio" name="type" id="static" value="static" class="event-type">
                    <label for="static"><span class="fi-loop"></span> Static</label>
                </div>

                <fieldset>
                    <div class="fdatepicker" style="">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="text" class="span2" name="start-date" value="{{$event->date_start}}" placeholder="Start date" id="dpd1">
                                    </th>
                                    <th>
                                        <input type="text" class="span2" value="{{$event->date_stop}}" name="end-date" placeholder="End date" id="dpd2">
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </fieldset>

                <textarea name="description" id="" cols="20" rows="10" placeholder="Description">{{$event->description}}</textarea>
                <div class="columns large-12" style="padding: 0;">
                  <input type="text" id="lat" value="" class="hide" name="latitude">
                  <input type="text" id="lng" value="" class="hide" name="longitude">
                  <input type="text" id="radius" value="" class="hide" name="radius">
                  <script type="text/javascript">
                    $("#rangeinput").on('change', function() {
                      $('#radius').attr("value", $('#rangevalue')[0].innerText);
                    });
                  </script>

                    <div class="slider">
                      <input type = "range" min="0" value="{{$event->radius}}" name="radius" max="500" onchange="rangevalue.value=value"/>
                      <output id="rangevalue" value="{{$event->radius}}">{{$event->radius}}</output>
                  </div>

                    <div id="map" class="half-map">
                        <div class="loader">Loading...</div>
                    </div>
                </div>
                <input type="submit" class="button large edit rounded" value="save" />
            </form>

        </div>
    </div>

    <script type="text/javascript">
        var elem = $('.equal');
        var equal = new Foundation.Equalizer(elem);

        $(document).on('change', '.upload-form', function () {
            $(this).submit();
        })
    </script>

    <!-- Datepicker -->
    <script src="/assets/js/foundation-datepicker.min.js"></script>
    <script type="text/javascript" src="/assets/js/datepicker.js"></script>

    <!-- Google Maps -->
    <script>
        var tooltip = new Foundation.Tooltip($('label.switch-paddle'));
    </script>

    <script type="text/javascript" src="/assets/js/maps.js"></script>
    <script async defer src="//maps.googleapis.com/maps/api/js?v=3&amp;key=AIzaSyBX7cJkAz8yUv4eBnosGlPPkHaEGRSHmV8&callback=initMap"></script>

    @endsection
