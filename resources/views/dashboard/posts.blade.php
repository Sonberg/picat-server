@foreach($posts as $post)
<div class="column preview @if($post->isVisible != 1) inactive @endif">
   <label for="{{$post->id}}">
    <img src="{{$post->url}}" class="thumbnail" alt="">
    <div class="bar">
        <span class="fi-torso"></span> {{$post->owner_user}}
        <span class="fi-clock"></span>
        <time class="timeago" datetime="{{$post->created_at}}">{{$post->created_at}}</time>
    </div>
    <input name="selected[]" id="{{$post->id}}" class="checkbox-post hide" type="checkbox" value="{{$post->id}}">
    <!--
   <div class="buttons">
       <input class='tgl tgl-skewed' id='cb{{$post->id}}' type='checkbox'>
        <label class='tgl-btn' data-tg-off='SHOW' data-tg-on='HIDE' for='cb{{$post->id}}'></label>
        <a class="secondary button date" href="#"> {{$post->updated_at}}</a>
        <a href="#" class="button alert"><span class="fi-x"></span></a>
   </div>
   -->
   </label>
</div>
@endforeach
