<canvas id="activity" height="300"></canvas>
    <div class="data hide">
       <script>
           $(document).ready(function() {
               var ctx = $("#activity").get(0).getContext("2d"),
                    lineChart = null;

                var options = {
                    animation: true,
                    animationEasing: "easeOutQuart",
                    bezierCurve: true,
                    scaleShowHorizontalLines: false,
                    showScale: false,
                    responsive: true,
                    tooltipCaretSize: 0,
                    pointDotRadius : 5,
                     pointDotStrokeWidth : 2,
                }

                   var data = {
                        labels: [],
                        datasets: [
                            {
                                label: "Upload Activity",
                                fillColor: "rgba(151,187,205,0.0)", // 0.2
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: []
                            }
                        ]
                    };
                    checkEnable();

                    function checkEnable() {
                       var week = $('.scale-activity.week').hasClass('active'),
                            month = $('.scale-activity.month').hasClass('active'),
                            year = $('.scale-activity.year').hasClass('active');

                       if(week) {
                            reset(lineChart);
                           @foreach($week as $key => $value)
                                data.labels.push(String("{{$key}}"));
                                data.datasets[0].data.push(String("{{$value}}"));
                            @endforeach
                       }

                        if(year) {
                            reset(lineChart);
                            @foreach($year as $key => $value)
                                data.labels.push(String("{{$key}}"));
                                data.datasets[0].data.push(({{$value}}).toString());
                            @endforeach
                       }
                        if(month) {
                            reset(lineChart);
                            @foreach($month as $key => $value)
                              data.labels.push(String("{{$key}}"));
                              data.datasets[0].data.push(({{$value}}).toString());
                            @endforeach
                        }

                        lineChart = new Chart(ctx).Line(data, options);
                       }
                   function reset(chart) {
                       if(chart !== null) {
                           chart.destroy();
                       }
                        data.labels = [];
                        data.datasets[0].data = [];
                   }

                    $(document).on('click', '.scale-activity', function () {
                        $('.scale-activity').removeClass('active');
                        $(this).addClass('active');
                        checkEnable();

                    })
            })
        </script>
    </div>
    <div class="button-group toggleActivity">
        <a class="button hollow scale-activity week">Week</a>
        <a class="button hollow active scale-activity month">Month</a>
        <a class="button hollow scale-activity year">Year</a>
    </div>
