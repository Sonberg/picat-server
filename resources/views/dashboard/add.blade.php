@extends('app') @section('content') @include('navbar')
<div class="container" onload="initMap()">
    <form method="post" class="add" action="" id="add" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <h1 class="title text-center" style="margin-top:100px">New event</h1>
        <h5 class="subheader">*required</h5>
        <div class="input-block">
            <span class="badge large">1</span>

            <!-- Choose name for event -->
            <fieldset class="">
                <legend>Basic information</legend>
                <input type="text" name="title" placeholder="Title*" id="title">
                <input type="text" name="url" placeholder="Website" id="url">
            </fieldset>

            <fieldset>
                <label>
                    <textarea placeholder="Description" name="desciption"></textarea>
                </label>
            </fieldset>


        </div>

        <!-- Logotype -->
        <div class="input-block">
            <span class="badge large">2</span>

            <!-- Choose name for event -->
            <fieldset class="">
                <legend>Add logotype</legend>
                <p>max 1 file</p>
                <div class="browse-wrap">
                    <div class="title">Choose file to upload*</div>
                    <input type="file" name="thumb" class="logotype-upload upload" placeholder="" id="logotype" accept="image/*">
                </div>
                <img id="thumb-preview">
            </fieldset>
        </div>


        <div class="input-block">
            <span class="badge large">3</span>

            <!-- Choose type of event -->
            <fieldset>
                <legend>Choose type of event*</legend>
                <input type="radio" name="type" value="timelimit" id="timelimit">
                <label for="timelimit">Timelimited</label>
                <input type="radio" name="type" value="static" id="static" checked>
                <label for="static">Static</label>
                @if($user->account_type == 'admin')
                  <input type="radio" name="type" value="student" id="student">
                  <label for="student">Student</label>
                @endif
            </fieldset>

            <fieldset>
                <div class="fdatepicker" style="">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    <input type="text" class="span2" name="start-date" value="" placeholder="Start date" id="dpd1" disabled>
                                </th>
                                <th>
                                    <input type="text" class="span2" value="" name="end-date" placeholder="End date" id="dpd2" disabled>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </fieldset>

        </div>
        <div class="input-block">
            <span class="badge large">4</span>
            <fieldset>
                <legend>Location*</legend>
                <p>Click on the map to mark the location for your event</p>
                 <div class="slider">
                      <input id="rangeinput" type="range" min="0" max="500" value="" oninput="rangevalue.value=value"/>
                      <output id="rangevalue">10</output>
                  </div>
                <input type="text" id="lat" value="" class="hide" name="latitude">
                <input type="text" id="lng" value="" class="hide" name="longitude">
                <input type="text" id="radius" value="" class="hide" name="radius">
                <script type="text/javascript">
                  $("#rangeinput").on('change', function() {
                    $('#radius').attr("value", $('#rangevalue')[0].innerText);
                  });
                </script>
            </fieldset>
            <div id="map" class="edit-map">
                <div class="loader">Loading...</div>
            </div>
        </div>
        <div class="input-block">
            <span class="badge large">5</span>
            <fieldset>
                <legend>Add watermark*</legend>
                <p>max 2 images</p>
                <div class="browse-wrap">
                    <div class="title">Choose files to upload</div>
                    <input type="file" name="upload[]" class="upload wm-upload" title="Choose files to upload" multiple accept="image/*">
                </div>
                <div id="list"></div>
                <div class="clear-upload button" onclick="reset()" style="display: none;"><span class="fi-x"></span> clear</div>
            </fieldset>
        </div>

        <div class="input-block">
            <fieldset>
                <input type="submit" value="Create event" class="button large rounded" id="submit">
            </fieldset>
        </div>

    </form>
</div>

<!-- Datepicker -->
<script src="/assets/js/foundation-datepicker.min.js"></script>
<script type="text/javascript" src="/assets/js/datepicker.js"></script>

<!-- Uploader -->
<script type="text/javascript" src="/assets/js/upload.js"></script>


<!-- Google Maps -->
<script type="text/javascript" src="/assets/js/maps.js"></script>
<script async defer src="//maps.googleapis.com/maps/api/js?key=AIzaSyBX7cJkAz8yUv4eBnosGlPPkHaEGRSHmV8&callback=initMap"></script>

@endsection
