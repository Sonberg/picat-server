@extends('app') @section('content')
<img src="/assets/img/icon-app.png" class="float-center" alt="">
<h1 class="text-center">{{trans('policy.title')}}</h1>
@include("component.language-select")
<div class="row policy u-pad-m">

    <p>{{trans('policy.intro-1')}}</p>

    <p>{{trans('policy.intro-2')}}</p>

    <h2>{{trans('policy.list-title')}}</h2>
    <ul>
        <li>{{trans('policy.list-1')}}</li>
        <li>{{trans('policy.list-2')}}</li>
        <li>{{trans('policy.list-3')}}</li>
        <li>{{trans('policy.list-4')}}</li>
        <li>{{trans('policy.list-5')}}</li>
        <li>{{trans('policy.list-6')}}</li>
        <li>{{trans('policy.list-7')}}</li>
        <li>{{trans('policy.list-8')}}</li>
        <li>{{trans('policy.list-9')}}</li>
        <li>{{trans('policy.list-10')}}</li>
        <li>{{trans('policy.list-11')}}</li>
        <li>{{trans('policy.list-12')}}</li>
        <li>{{trans('policy.list-13')}}</li>
        <li>{{trans('policy.list-14')}}</li>
        <li>{{trans('policy.list-15')}}</li>
        <li>{{trans('policy.list-16')}}</li>


    </ul>
    <p></p>
    <h2>{{trans('policy.info-title')}}</h2>
    <p>{{trans('policy.info-1')}}</p>
    <p>{{trans('policy.info-2')}}</p>
    <p>{{trans('policy.info-3')}}</p>
    <p>{{trans('policy.info-4')}}</p>
    <p>{{trans('policy.info-5')}}</p>
    <p>{{trans('policy.info-6')}}</p>
    <p>{{trans('policy.info-7')}}</p>
    <p>{{trans('policy.info-8')}}</p>
    <p>{{trans('policy.info-9')}}</p>
    <p>{{trans('policy.info-10')}}</p>





    <h2>{{trans('policy.use-title')}}</h2>
    <p>{{trans('policy.use-1')}}</p>
    <p>{{trans('policy.use-2')}}</p>

    <h2>{{trans('policy.share-title')}}</h2>
    <p>{{trans('policy.share-1')}}</p>
    <p>{{trans('policy.share-2')}}</p>
    <p>{{trans('policy.share-3')}}</p>
    <p>{{trans('policy.share-4')}}</p>

    <h2>{{trans('policy.store-title')}}</h2>
    <p>{{trans('policy.store-1')}}</p>

    <h2>{{trans('policy.choose-title')}}</h2>
    <p>{{trans('policy.choose-1')}}</p>

    <h2>{{trans('policy.sekretess-title')}}</h2>
    <p>{{trans('policy.sekretess-1')}}</p>


    <h2>{{trans('policy.service-title')}}</h2>
    <p>{{trans('policy.service-1')}}</p>


    <h2>{{trans('policy.dead-title')}}</h2>
    <p>{{trans('policy.dead-1')}}</p>


    <h2>{{trans('policy.contact-title')}}</h2>
    <p>{{trans('policy.contact-1')}}</p>


    <h2>{{trans('policy.change-title')}}</h2>
    <p>{{trans('policy.change-1')}}</p>
</div>
@endsection
