<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('isVisible')->default(true);
            $table->boolean('postsIsVisible')->default(true);
            $table->string('type')->default('event');
            $table->unsignedInteger('owner_company');
            $table->string('title', 255);
            $table->string('url', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('event_type', 255);
            $table->string('thumb',255)->nullable();
            $table->decimal('latitude', 23, 20);
            $table->decimal('longitude', 23, 20);
            $table->integer('radius')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('date_start')->nullable();
            $table->string('date_stop')->nullable();
        });
        
        Schema::table('events', function($table) {
            $table->foreign('owner_company')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
