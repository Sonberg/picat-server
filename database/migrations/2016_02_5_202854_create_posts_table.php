<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->boolean('isVisible')->default(false);
            $table->unsignedInteger('owner_event');
            $table->unsignedInteger('owner_user');
            $table->string('owner_type', 20);
            $table->string('text', 255)->nullable();
            $table->string('url', 255);
            $table->softDeletes();
        });
        
        Schema::table('posts', function($table) {
            $table->foreign('owner_event')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('owner_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
