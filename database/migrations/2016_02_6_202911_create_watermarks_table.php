<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWatermarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('watermarks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255);
            $table->boolean('isVisible')->default(true);
            $table->unsignedInteger('owner_event');
            $table->unsignedInteger('owner_user');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('watermarks', function($table) {
            $table->foreign('owner_event')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('owner_user')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('watermarks');
    }
}
