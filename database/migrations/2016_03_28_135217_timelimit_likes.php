<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimelimitLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timelimit_likes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('owner_user');
            $table->unsignedInteger('post');
            $table->timestamps();
        });
        
        Schema::table('timelimit_likes', function($table) {
            $table->foreign('owner_user')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('post')->references('id')->on('timelimit_posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timelimit_likes');
    }
}
