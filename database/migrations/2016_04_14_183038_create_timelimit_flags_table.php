<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelimitFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timelimit_flags', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('user');
          $table->unsignedInteger('post');
          $table->string("text");
          $table->timestamps();
          $table->softDeletes();
        });

        Schema::table('timelimit_flags', function (Blueprint $table) {
          $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
          $table->foreign('post')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('timelimit_flags');
    }
}
