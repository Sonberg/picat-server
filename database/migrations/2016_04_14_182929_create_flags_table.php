<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user');
            $table->unsignedInteger('post');
            $table->string("text");
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('flags', function (Blueprint $table) {
          $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
          $table->foreign('post')->references('id')->on('timelimit_posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flags');
    }
}
