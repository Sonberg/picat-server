<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = 'posts';
    protected $fillable = array('isVisible', 'text', 'url');
    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo('App/User');
    }
    
    public function festivity() {
        return $this->belongsTo('App/Festivity');
    }

}
