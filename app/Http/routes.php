<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localize' ]], function() {

  /*
    INDEX
    Unprotected routes
  */

  Route::get('/', 'IndexController@index');
  Route::get('/policy', 'IndexController@getPolicy');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['api']], function () {

    /*
        All API-Routes
    */

    Route::get('/api', 'ApiController@index');


    /*
      Logging user in database and returnings uber-objekt
    */
    Route::post('/api/user/{fb}', 'ApiController@getUser');


    /*
      User likes post in-app
    */
    Route::get('/api/post/{id}/like/{user}/{type?}', 'LikeController@getLike');

    /*
      User flags a post in-app
    */
    Route::post('/api/post/{id}/flag/{fb}/{type?}', 'FlagController@getFlag');

    /*
      Return watermarks, depend on coordinates
    */
    Route::get('/api/watermarks/{latitude}/{longitude}', 'ApiController@getWatermarks');

    /*
      Return all events for event view
    */
    Route::get('/api/events/{latitude}/{longitude}', 'ApiController@getAllEvents');

    /*
      Return all posts for an event
    */
    Route::get('/api/event/{type}/{id}/{fb}/{last}', 'ApiController@getEventPosts');

    // Student
    Route::get('/api/schools/{latitude}/{longitude}', 'ApiController@getAllSchools');

    // Timelimit
    Route::get('/api/timelimits/{latitude}/{longitude}', 'ApiController@getAllTimelimits');

    // User uploads a new image
    Route::post('/api/photos/image/{type}', 'ApiController@postImage');

});

Route::group(['middleware' => ['web', 'auth', 'activated', 'admin']], function () {

    /*
        ADMIN
        admin protected middleware
    */

    /*
      View all costumers and edit
    */
    Route::get('/dashboard/companies', 'AdminController@getCompanies');
    Route::post('/dashboard/companies', 'AdminController@postCompanies');

    /* Promote / Delete a company*/
    Route::post('/dashboard/company/delete', 'AdminController@postDeleteCompany');
    Route::post('/dashboard/company/promote', 'AdminController@postPromoteCompany');

    /* Create / Edit / Delete timelimit WATERMARKS */
    Route::get('/dashboard/timelimit', 'AdminController@getTimelimit');
    Route::post('/dashboard/timelimit', 'AdminController@postTimelimit');
    Route::post('/dashboard/timelimit/delete', 'AdminController@postDeleteTimelimit');

    /* Old */
    Route::get('/dashboard/countries', 'AdminController@getCountries');
    Route::post('/dashboard/countries', 'AdminController@postCountries');
    Route::post('/dashboard/countries/delete', 'AdminController@postDeleteCountry');

    /*
      Handel flagged posts
    */
    Route::get('/dashboard/flag', 'AdminController@getFlag');
    Route::post('/dashboard/flag', 'AdminController@postFlag');

    Route::get('/dashboard/events', 'AdminController@getEvents');


});

Route::group(['middleware' => ['web', 'auth', 'activated']], function () {

    /*
        AUTH
        dashboard for regulary companies
    */
    Route::get('/logout', 'DashboardController@getLogout');

    /*
        DASHBOARD
    */

    // Index page
    Route::get('/dashboard', 'DashboardController@getIndex');

    // Add new event
    Route::get('/dashboard/add', 'DashboardController@getAdd');
    Route::post('/dashboard/add', 'DashboardController@postAdd');

    // Edit event
    Route::get('/dashboard/event/{id}', 'EventController@getEvent');
    Route::get('/dashboard/event/{id}/edit', 'EventController@getEventEdit');
    Route::post('/dashboard/event/{id}/edit', 'EventController@postEventEdit');
    Route::get('/dashboard/event/{id}/delete', 'EventController@getDeleteEvent');

    // Quick settings
    Route::post('/dashboard/event/{id}/update/visibility', 'EventController@postEventUpdateVisibility');
    Route::post('/dashboard/event/{id}/update/new-posts', 'EventController@postEventUpdateNewPosts');

    // Fetch posts
    Route::get('/dashboard/posts/{id}', 'EventController@getPosts');

    // Edit post
    Route::post('/dashboard/posts/{id}/edit', 'EventController@postPosts');

    // Remove Image
    Route::post('/dashboard/event/{id}/image/delete', 'EventController@postDeleteImage');
    Route::post('/dashboard/event/{id}/image/upload', 'EventController@postUploadImage');

    Route::get('/dashboard/settings', 'DashboardController@getSettings');
    Route::post('/dashboard/settings', 'DashboardController@postSettings');
});


Route::group(['middleware' => ['web']], function () {

    /*
        AUTH
    */
    Route::get('/auth/login', 'Auth\AuthController@getLogin');
    Route::post('/auth/login', 'Auth\AuthController@postLogin');
    Route::get('/auth/logout', 'Auth\AuthController@getLogout');

    Route::get('/auth/register', 'Auth\AuthController@getRegister');
    Route::post('/auth/register', 'Auth\AuthController@postRegister');

    Route::get('/auth/awaiting', 'Auth\AuthController@getAwaiting');
});
