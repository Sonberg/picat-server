<?php

namespace App\Http\Middleware;

use Request;
use Closure;
use App\Http\Requests;

class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // X-Token = ya0lhlyzmbkpvk1bgckehfd1ym1tb7aii5wd8gp7
        $token = Request::header('X-Token');
        if($token === 'ya0lhlyzmbkpvk1bgckehfd1ym1tb7aii5wd8gp7') {
            return $next($request);
        } else {
            return response('Unauthorized.', 401);
        }
    }
}
