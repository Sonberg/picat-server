<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Activated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if(Auth::User()->isActive === 0 && Auth::User()->account_type === 'company') {
                return redirect('/auth/awaiting');    
            } else {
                return $next($request);
            }
    }
}
