<?php

namespace App\Http\Middleware;

use Closure;

class secureHTTP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if( $_SERVER['HTTP_HOST'] == "localhost:8888" || $this->isSecure()) {
          return $next($request);
        }
        $this->requireHTTPS();
    }

    public function isSecure() {
      return (
          (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
       || $_SERVER['SERVER_PORT'] == 443
       || (
              (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
           || (!empty($_SERVER['HTTP_X_FORWARDED_SSL'])   && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
          )
      );
    }

    public function requireHTTPS() {
      if (!$this->isSecure()) {
          header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], TRUE, 301);
          exit;
      }
    }
}
