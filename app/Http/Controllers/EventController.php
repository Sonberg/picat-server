<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Input;
use App\S3;
use DateTime;
use Response;
use App\Post;
use App\Activity;
use App\Festivity;
use App\Watermark;
use Aws\S3\S3Client;
use App\Http\Requests;
use App\SessionHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditEventRequest;
use App\Http\Requests\CreateEventRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EventController extends Controller
{

    /*
        VIEW EVENT PAGE
    */
    public function getEvent($id) {
        $user = Auth::user();
        $event = Festivity::where('id', '=', $id)->first();

        if ($user->type == 'company' && $event->owner_company != $user->id) {
          session(['error' => 'Event not found or you dont have premission']);
          return redirect('/dashboard');
        }

        if ($event) {
            $posts = Post::where('owner_event', $event->id)->get();
            $postcount = Post::where('owner_event', $event->id)->count();
            $watermarks = Watermark::where('owner_event', $event->id)->get();
            $events  = Festivity::where('owner_company', $user['id'])->orderBy('title')->get();

            $year = Activity::calcYear($event->id);
            $month = Activity::calcMonth($event->id);
            $week = Activity::calcWeek($event->id);


            return view('dashboard/view', [
                'postcount' => $postcount,
                'events' => $events,
                'flagged' => parent::flagCount(),
                'event' => $event,
                'watermarks' => $watermarks,
                'user' => $user,
                'year' => $year,
                'month' => $month,
                'week' => $week,
                'flash' => SessionHandler::all(),
            ]);
        }

        session(['error' => 'Event not found or you dont have premission']);
        return redirect('/dashboard');
    }

    /*
        DELETE EVENT
    */

    public function getDeleteEvent($id) {
        $watermarks = Watermark::where('owner_event', '=', $id)->get();
        foreach($watermarks as $w) {
            $w->delete();
        }

        $event = Festivity::find($id);
        $event->delete();
        session(['success' => 'Event was successfullt deleted']);
        return redirect('dashboard');
    }

    /*
        EDIT EVENT
    */

    public function getEventEdit($id) {
        $user = Auth::user();
        $event = Festivity::where('id', '=', $id)->first();

        if ($user->type == 'company' && $event->owner_company != $user->id) {
          session(['error' => 'Event not found or you dont have premission']);
          return redirect('/dashboard');
        }

        if ($event != null) {
            $watermarks = Watermark::where('owner_event', $event->id)->limit(2)->get();
            $events  = Festivity::where('owner_company', $user['id'])->orderBy('title')->get();

            return view('dashboard/edit', [
                'user' => $user,
                'event'=> $event,
                'flagged' => parent::flagCount(),
                'events' => $events,
                'watermarks' => $watermarks,
                'flash' => SessionHandler::all()
            ]);
        }

    }

    public function postEventEdit(EditEventRequest $request, $id) {
        $user = Auth::user();
        $event = Festivity::where('id', '=', $id)->first();
        $input = $request->all();

        if ($user->type == 'company' && $event->owner_company != $user->id) {
          session(['error' => 'Event not found or you dont have premission']);
          return redirect('/dashboard');
        }

        if ($event != null) {
            $event->title = array_get($input,'title');
            $event->url = array_get($input,'url');

            $event->description = array_get($input, 'description');
            $event->event_type = array_get($input, 'type');

            // Remove unneeded dates
            if(array_get($input, 'type') !== 'timelimit') {
                $event->date_start = null;
                $event->date_stop = null;
            } else {
                $event->date_start = array_get($input,'start-date');
                $event->date_stop = array_get($input,'end-date');
            }
            $event->radius = array_get($input,'radius');
            $event->latitude = array_get($input,'latitude');
            $event->longitude = array_get($input,'longitude');
            $event->save();

            session(['success' => 'Event was successfully updated']);
            return back();
        }
    }

    /*
        GET POSTS FOR EVENTS
    */

    public function getPosts($id) {
        $user = Auth::user();
        $event = Festivity::where('id', '=', $id)->first();
        $posts = Post::where('owner_event', $event->id)->orderBy('created_at', 'desc')->Paginate(12);

        if (Post::where('owner_event', $event->id)->count() == 0) {
          return false;
        }

        return view('dashboard/posts', [
            'posts'=> $posts,
            'flash' => SessionHandler::all()
        ]);
    }

    /*
        EDIT POSTS FOR EVENT
    */

     public function postPosts($id) {
        $input = Input::all();
        $user = Auth::user();

        $event = Festivity::where('owner_company', '=', $user->id)->where('id', '=', $id)
            ->firstOrFail();

        if ($event->owner_company != $user->id) {
            session(['error' => 'Event not found or you dont have premission']);
            return redirect('/dashboard');
        } else {
            $selected = array_get($input, 'selected');

            if($selected == null) { return redirect()->back(); }

            $show = array_get($input, 'show_button');
            $hide = array_get($input, 'hide_button');
            $delete = array_get($input, 'delete_posts');

            foreach($selected as $item) {
                $post = Post::findOrFail($item);

                if($show !== null) {
                    $post->isVisible = 1;
                    $post->save();
                }
                else if($hide !== null) {
                    $post->isVisible = 0;
                    $post->save();
                }
                else if($delete !== null) {
                    $post->delete();
                }
            }


            return redirect('dashboard/event/' . $event->id);
        }
    }


    /*
        UPDATE EVENT
    */
    public function postEvent($id, EditEventRequest $request) {
        $input = $request->all();
        $event = Festivity::where('owner_company', '=', $user->id)->where('id', '=', $id)->first();

        $fields = ['isVisible', 'owner_company', 'title', 'url', 'description', 'event_type', 'latitude', 'longitude', 'date_start', 'date_stop'];

        foreach($fields as $field) {
            $user = array_get($input, $field);
            $DB = $event[$field];

            if($user !== null && $user !== $DB) {
                $event->$field = $user;

                if($field === 'event_type' && $user === 'timelimit') {
                    $event->$date_start = array_get($input, 'date_start');
                    $event->$date_stop = array_get($input, 'date_stop');
                }

                if($field === 'event_type' && $user === 'static') {
                    $event->$date_start = null;
                    $event->$date_stop = null;
                }

            }
        }
        $event->save();
        return redirect('/dashboard/event/'.$id);
    }


    /*
        DELETE IMAGE
    */

    public function postDeleteImage ($id) {
        $input = Input::all();
        $user = Auth::user();

        $event = Festivity::where('id', '=', $id)->first();

        if ($event->owner_company != $user->id && $user->type != "admin") {
            session(['error' => 'Event not found or you dont have premission']);
            return redirect('/dashboard');
        } else {
            $userId = $user['id'];
            $url = array_get($input, 'url');
            $filename = explode("/", $url);
            $type = array_get($input, 'type');

            if($type === 'logotype') {
                $event->thumb = null;
                S3::delete(array_pop($filename), 'logotypes');
                $event->save();
            } else if ($type === 'watermarks') {
                $watermark = Watermark::where('owner_event', '=', $id)
                    ->where('owner_user', '=', $userId)
                    ->where('url', '=', $url)
                    ->firstOrFail();
                S3::delete(array_pop($filename), 'watermarks');
                $watermark->delete();
            } else {
                // error
                return;
            }

            session(['success' => 'Image was successfull deleted']);
            return redirect('/dashboard/event/'.$id.'/edit');
        }

    }


    /*
        UPLOAD IMAGE
    */

    public function postUploadImage ($id) {
        $input = Input::all();
        $user = Auth::user();
        $event = Festivity::where('id', '=', $id)->first();

        if ($event->owner_company != $user->id && $user->type != "admin") {
            session(['error' => 'Event not found or you dont have premission']);
            return redirect('/dashboard');
        } else {
            $type = array_get($input, 'type');

            if($type === 'logotype') {
                $event->thumb = S3::put(array_get($input, 'thumb'), 'events/'.$user->id.'/logotypes');
                $event->save();

            } else if ($type === 'watermarks') {
                $watermark = new Watermark;
                var_dump($input);
                $watermark->url = S3::put(array_get($input, 'url'), 'events/'.$user->id.'/watermarks');
                $watermark->owner_event = $event->id;
                $watermark->owner_user = $user->id;
                $watermark->save();

            } else {
                session(['success' => 'Faliure to upload image']);
                return redirect('/dashboard/event/'.$id.'/edit');
            }

            session(['success' => 'Image was successfull uploaded']);
            return redirect('/dashboard/event/'.$id.'/edit');
        }
    }

    /*
        UPDATE EVENT QUICK SETTINGS
    */

    public function postEventUpdateVisibility($id) {
        $input = Input::all();
        $user = Auth::user();
        $state = array_get($input, 'eventStatus');
        $event = Festivity::where('id', '=', $id)->first();
        if($event !== null) {
            if($state === 'hidden') {
                $event->isVisible = 0;
            } else {
                $event->isVisible = 1;
            }
            $event->save();
            session(['success' => 'Changes saved']);
            return redirect()->back();
        } else {
            session(['error' => 'Changes is not saved']);
            return redirect()->back();
        }
    }

    public function postEventUpdateNewPosts ($id) {
        $input = Input::all();
        $user = Auth::user();
        $state = array_get($input, 'newPostStatus');
        $event = Festivity::where('id', '=', $id)->first();
        var_dump($input);
        if($event !== null) {
            if($state === 'hidden') {
                $event->postsIsVisible = 0;
            } else {
                $event->postsIsVisible = 1;
            }
            $event->save();
            session(['success' => 'Changes saved']);
            return redirect()->back();
        } else {
            session(['error' => 'Changes is not saved']);
            return redirect()->back();
        }
    }
}
