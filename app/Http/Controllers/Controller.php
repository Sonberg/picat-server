<?php

namespace App\Http\Controllers;

use Auth;
use App\Flag;
use App\Person;
use App\TimelimitFlag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function returnUser($fb) {
      return Person::where('facebook_id', '=', $fb)->get()->first();
    }

    public function flagCount() {
      $user = Auth::user();
      if($user->type != 'admin') {
        return null;
      }
      $flagged = Flag::where('deleted_at', '=', NULL)->get()->toArray();
      $time = TimelimitFlag::where('deleted_at', '=', NULL)->get()->toArray();
      return array_merge($flagged, $time);

    }
}
