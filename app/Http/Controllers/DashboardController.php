<?php

namespace App\Http\Controllers;

use DB;
use Hash;
use Auth;
use Input;
use App\S3;
use DateTime;
use Response;
use App\Post;
use App\Activity;
use App\Festivity;
use App\Watermark;
use App\SessionHandler;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\CreateEventRequest;

class DashboardController extends Controller
{

    /*
        VIEW INDEX PAGE FOR DASHBOARD
    */
    public function getIndex() {
        $user = Auth::user();
        $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();
        return view('dashboard/index', [
            'user' => $user,
            'events'=> $events,
            'flagged' => parent::flagCount(),
            'flash' => SessionHandler::all()
        ]);
    }

    /*
        VIEW ADD PAGE
    */
    public function getAdd() {
        $user = Auth::user();
        $events  = Festivity::where('owner_company', $user['id'])->orderBy('title')->get();

        return view('dashboard/add', [
            'user' => $user,
            'events'=> $events,
            'flagged' => parent::flagCount(),
            'flash' => SessionHandler::all()
        ]);
    }

    /*
        ADD EVENT TO DATABAS
    */
    public function postAdd(CreateEventRequest $request) {
        $input = $request->all();
        $user = Auth::user();
        $logoDest = 'uploads/' . Auth::user()["id"]. '/logotype';
        $data = new Festivity;

        $data->owner_company = Auth::user()['id'];
        $data->title = array_get($input,'title');
        $data->url = array_get($input,'url');
        $data->radius = array_get($input, 'radius');

        // Upload logotype
        $data->thumb = S3::put(array_get($input, 'thumb'), 'events/'.$user->id.'/logotyps');
        $data->description = array_get($input, 'description');
        $data->event_type = array_get($input, 'type');

        // Remove unneeded dates
        if(array_get($input, 'type') !== 'timelimit') {
            $data->date_start = null;
            $data->date_stop = null;
        } else {
            $data->date_start = array_get($input,'start-date');
            $data->date_stop = array_get($input,'end-date');
        }

        $data->latitude = array_get($input,'latitude');
        $data->longitude = array_get($input,'longitude');

        if($data->save()) {
            $response = Response::json(array('success' => true, 'last_insert_id' => $data->id), 200);

            // Upload watermarks
            $images = array_get($input,'upload');

            foreach ($images as $image) {


                $water = new Watermark;
                $water->url = S3::put($image, 'events/'.$user->id.'/watermarks');
                $water->isVisible = true;
                $water->owner_event = json_decode($response->getContent())->last_insert_id;
                $water->owner_user = Auth::user()['id'];
                $water->save();
            }
            session(['Success' => 'Event was succesfull created']);
            return redirect('/dashboard/event/' . json_decode($response->getContent())->last_insert_id);
        }
    }


    /*
        GET EVENT SETTINGS
    */

    public function getSettings() {
        $user = Auth::user();
        $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();
        return view('dashboard/settings', [
            'user' => $user,
            'events'=> $events,
            'flagged' => parent::flagCount(),
            'flash' => SessionHandler::all()
        ]);
    }

    public function postSettings(UpdateUserRequest $request) {
      $input = $request->all();
      $user = Auth::user();
      $name = array_get($input , 'name');
      $email = array_get($input , 'email');
      $phone = array_get($input , 'phone');
      $new = array_get($input , 'new-password');
      $newRepat = array_get($input , 'new-password-repeat');
      $old = array_get($input , 'old-password');

      if ($name) {
        $user->name = $name;
      }

      if ($email) {
        $user->email = $email;
      }

      if ($phone) {
        $user->number = $phone;
      }

      if ($new && $old) {
        echo "new";
        if (!Hash::check($old, $user->password)) {
          session(['Error' => 'The old password is matching']);
          return redirect()->back();
        }
        if($new == $newRepat) {
          $user->password = Hash::make($new);
        }

      }

      $user->save();
      session(['Success' => 'Changes was successfully saved!']);
      return redirect()->back();
    }

    public function getLogout() {
        Auth::logout();
        return redirect("/auth/login");
    }

}
