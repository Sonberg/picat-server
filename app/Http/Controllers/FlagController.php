<?php

namespace App\Http\Controllers;

use Input;
use App\Flag;
use App\Person;
use App\TimelimitFlag;
use App\Http\Requests;
use Illuminate\Http\Request;

class FlagController extends Controller
{
    public function getFlag($id, $fb, $type) {
      $user = Person::where(['facebook_id' => $fb])->get()->first();
      $input = Input::all();

      if ($type == "timelimit") {
        $flag = new TimelimitFlag();
      } else {
        $flag = new Flag();
      }
      $flag->user = $user->id;
      $flag->post = $id;
      $flag->text = array_get($input, "text");
      $flag->save();
      return $flag;
    }
}
