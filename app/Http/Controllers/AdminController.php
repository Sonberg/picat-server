<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Input;
use App\S3;
use App\Person;
use App\Flag;
use DateTime;
use Response;
use App\User;
use App\Post;
use App\Country;
use App\Activity;
use App\Timelimit;
use App\Festivity;
use App\Watermark;
use Carbon\Carbon;
use App\TimelimitPost;
use App\TimelimitFlag;
use App\Http\Requests;
use App\SessionHandler;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\EditEventRequest;

class AdminController extends Controller {

    /*
        ALL COMPANIES
    */

    public function getCompanies() {
        $user = Auth::user();
        $users = User::all();
        $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();

        return view('dashboard/admin/companies', [
            'users' => $users,
            'user' => $user,
            'events' => $events,
            'flagged' => parent::flagCount(),
            'flash' => SessionHandler::all(),
        ]);
    }

    public function postCompanies() {
        $input = Input::all();
        $companies = User::all();

        for($i=0;$i<count($companies);$i++) {
            $u = $companies[$i];
            if($u !== NULL) {
                if(array_get($input, ($u->id)) === 'on') {
                   $u->isActive = '1';
                } else {
                    $u->isActive = '0';
                }
                $u->save();
            }
        }
        session(['success' => 'Changed saved']);
        return redirect()->back();
    }

    /*
        EDIT COMPANY
    */

    public function postDeleteCompany() {
        $input = Input::all();
        User::destroy(array_get($input, 'id'));

        session(['success' => 'Company removed']);
        return redirect()->back();
    }

    public function postPromoteCompany() {
        $input = Input::all();
        $user = parent::returnUser(array_get($input, 'id'));

        if($user->account_type === 'company') {
            $user->account_type = 'admin';
            $user->save();

            session(['success' => 'Changes saved']);
            return redirect()->back();

        } else if($user->account_type === 'admin') {
            $user->account_type = 'company';
            $user->save();

            session(['success' => 'Changes saved']);
            return redirect()->back();

        } else {
            session(['error' => 'There was an error']);
            return redirect()->back();
        }
    }


    /*
        COUNTRY WATERMARKS
    */

    public function getCountries() {
        $user = Auth::user();
        $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();
        $countries = Country::All();

        return view('dashboard/admin/countries', [
            'user' => $user,
            'events' => $events,
            'flagged' => parent::flagCount(),
            'countries' => $countries,
            'flash' => SessionHandler::all(),
        ]);
    }

    public function postCountries() {
        $input = Input::all();
        $c = new Country;
        $c->country = array_get($input, 'title');
        $c->short_name = array_get($input, 'short');
        $c->url = S3::put(array_get($input, 'thumb'), 'global');
        $c->save();

        session(['success' => 'Watermark created']);
        return redirect()->back();
    }

    public function postDeleteCountry() {
        $input = Input::all();
        Country::destroy(array_get($input, 'id'));

        session(['success' => 'Watermark was successfully deleted']);
        return redirect()->back();
    }

    /*
        TIMELIMITED WATERMARKS
    */

    public function getTimelimit() {
        $user = Auth::user();
        $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();
        $timelimit = Timelimit::All();

        return view('dashboard/admin/timelimit', [
            'user' => $user,
            'events' => $events,
            'flagged' => parent::flagCount(),
            'timelimit' => $timelimit,
            'flash' => SessionHandler::all(),
        ]);
    }

    public function postTimelimit() {
        $input = Input::all();
        $t = new Timelimit;
        $t->title = array_get($input, 'title');

        $parts = explode('/', array_get($input, 'date_start'));
        $t->date_start = $parts[2] . $parts[0] . $parts[1];

        $parts = explode('/', array_get($input, 'date_stop'));
        $t->date_stop = $parts[2]  . $parts[0] . $parts[1];

        $t->url = S3::put(array_get($input, 'thumb'), 'timelimit');
        $t->save();



        session(['success' => 'Watermark created']);
        return redirect()->back();
    }

    public function postDeleteTimelimit() {
        $input = Input::all();
        Timelimit::destroy(array_get($input, 'id'));

        session(['success' => 'Watermark was successfully deleted']);
        return redirect()->back();
    }

    public function getFlag() {
      $user = Auth::user();
      $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();

      $flagged = Flag::where('deleted_at', '=', NULL)->get()->toArray();
      for($i=0;$i<count($flagged);$i++) {
        $flagged[$i]["type"] = "post";
        $post = Post::find($flagged[$i]["post"]);
        if($post != null) {
          $flagged[$i]["image"] = $post->url;
          $flagged[$i]["flag_by"] = Person::find($flagged[$i]['user'])->name;
          $flagged[$i]["post_by"] = Person::find($post->owner_user)->name;
        } else {
          Flag::destroy($flagged[$i]["id"]);
        }
      }

      $time = TimelimitFlag::where('deleted_at', '=', NULL)->get()->toArray();
      for($i=0;$i<count($time);$i++) {
        $time[$i]["type"] = "timelimit";
        $post = TimelimitPost::find($time[$i]["post"]);
        if($post != null) {
          $time[$i]["image"] = $post->url;
          $time[$i]["flag_by"] = Person::find($time[$i]['user'])->name;
          $time[$i]["post_by"] = Person::find($post->owner_user)->name;
        } else {
          TimelimitFlag::destroy($time[$i]["id"]);
        }
      }

      $flagged = array_merge($flagged, $time);
      usort($flagged, function ( $a, $b ) {
        return strtotime($a["created_at"]) - strtotime($b["created_at"]);
      });

      return view('dashboard.admin.flag', [
        'user' => $user,
        'events' => $events,
        'flagged' => $flagged,
        'flash' => SessionHandler::all()
      ]);
    }

    public function postFlag() {
      $input = Input::all();

      if(array_get($input, 'type') == "timelimit") {
        $flag = TimelimitFlag::find(array_get($input, 'id'));
        $post = TimelimitPost::find(array_get($input, 'post'));
        var_dump($post);
      } else {
        $flag = Flag::find(array_get($input, 'id'));
        $post = Post::find(array_get($input, 'post'));
      }
      $user = Person::find($post->owner_user);

      if($post == NULL) {
        return redirect()->back();
        session(['Error' => 'Something unexpected happend ypu should ask Per, he is the best']);
      }

      if(array_get($input, 'action') == "block") {
        $user->hasAccess = 0;
        $user->save();
        session(['success' => 'User was successfully blocked']);
      }

      if(array_get($input, 'action') == "remove") {
        $post->delete();
        session(['success' => 'Post was successfully deleted']);
      }

      $flag->delete();
      return redirect()->back();
    }

    public function getEvents() {
      $user = Auth::user();
      $events  = Festivity::where('owner_company', Auth::user()['id'])->orderBy('title')->get();
      $eventsAll = Festivity::all();

      return view('dashboard.admin.events', [
        'user' => $user,
        'events' => $events,
        'eventsAll' => $eventsAll,
        'flagged' => parent::flagCount(),
        'flash' => SessionHandler::all()
      ]);
    }
}
