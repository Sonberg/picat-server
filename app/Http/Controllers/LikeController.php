<?php

namespace App\Http\Controllers;

use App\Person;
use App\Like;
use App\TimelimitLike;
use App\Http\Requests;
use Illuminate\Http\Request;

class LikeController extends Controller {
  /*
      LIKE
  */

  public function getLike($id, $fb, $type) {
      $user = parent::returnUser($fb);

      if ($type == "timelimit") {
          $like = TimelimitLike::where('post', '=', $id)
              ->where('owner_user', '=', $user["id"])
              ->first();
      } else {
          $like = Like::where('post', '=', $id)
              ->where('owner_user', '=', $user["id"])
              ->first();
      }

      if($like == NULL) {
          // Post not liked
          if ($type == "timelimit") {
              $like = TimelimitLike::create(["post" => $id, "owner_user" => $user["id"]]);
          } else {
              $like = Like::create(["post" => $id, "owner_user" => $user["id"]]);
          }
          return array("liked" => true);

      } else {
          // Post liked
          $like->delete();
          return array("liked" => false);
      }
  }
}
