<?php

namespace App\Http\Controllers;

use DB;
use Input;
use App\Distance;
use Carbon\Carbon;
use Aws\S3\S3Client;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/* Models */
use App\S3;
use App\Post;
use App\Like;
use App\Event;
use App\Person;
use App\Country;
use App\Watermark;
use App\Timelimit;
use App\Festivity;
use App\TimelimitPost;
use App\TimelimitLike;
use App\CountryPost;

/* Request */
use App\Http\Requests\CreateUserRequest;

// Owner_type : String = event/timelimit/country

class ApiController extends Controller
{
    /*
        INDEX
        -> All posts
    */
     public function index() {
        $posts = DB::table('posts')->get();
        return $posts;
    }

    /*
        USER
        Retrive facebook id from client. Checks if its in the databas. Return user object
    */
    public function getUser(CreateUserRequest $request, $fb) {
        $input = $request->all();
        $user = parent::returnUser($fb);

        if($user == NULL) {
          $user = new Person;
          $user->hasAccess == 1;
        }
        $user->facebook_id = $fb;
        $user->name = array_get($input, 'name');
        $user->email = array_get($input, 'email');
        $user->save();
        $user = parent::returnUser($fb);

        // Deterend if user is blocked
        if ($user->hasAccess == 0) {
          return array("access" => false, "user" => $user);
        } else {
          return array("access" => true, "user" => $user);
        }
    }

    /*
        UPLOAD POST
    */

    public function postImage($type) {
        $input = Input::all();

        // Date for storeage
        $year = (Int) Carbon::now()->format('Y');
        $month = (Int) Carbon::now()->format('m');

        $user = parent::returnUser(array_get($input, 'owner_user'));
        //Person::where("id", "=", array_get($input, 'owner_user'))->get()->first();

        // What kind if picture is it?
        if(array_get($input, 'type') == "timelimit") {
            $owner = Timelimit::find(array_get($input, 'owner'));
            $path = 'posts/timelimit/'.$owner->id.'/'.$year.'/'.$month;
            $post = new TimelimitPost;
        } else {
            $owner = Festivity::where('id', '=', array_get($input, 'owner'))->get()->first();
            $path = 'posts/'.$owner->owner_company.'/'.$owner->id.'/'.$year.'/'.$month;
            $post = new Post;
        }

        $post->owner_event = $owner->id;
        $post->owner_type = $type;
        $post->text = array_get($input, 'text');
        $post->owner_user = $user->id;

        switch ($owner->postsIsVisible) {
            case 0:
                $post->isVisible = 0;
                break;
            case 1:
                $post->isVisible = 1;
                break;
            default:
                $post->isVisible = 1;

        }

        // Upload file to our S3 server
        $post->url = S3::put(array_get($input, 'image'), $path);
        $post->save();
        return $input;
    }

    /*
        WATERMARKS
        return watermarks based on location
    */

    public function getWatermarks($latitude, $longitude) {
        /*
          All active events. Check distance from our location. Grabs the closests within range
        */
        $events = Festivity::where('isVisible', '=', '1')->get()->toArray();
        $event = Distance::calcClosestEvent($events, $latitude, $longitude);

        /* Country is old */
        $country = null;
        //Country::where('short_name', '=', Distance::getCurrentCountry($latitude, $longitude))->first();
        if(!$country) { $country = null; }

        /*
         Find 2 active Timelimit watermarks and return to client
        */
        $today = (Int) Carbon::now()->format('Ymd');
        $timelimit = Timelimit::where("date_start", "<=", $today)
            ->where("date_stop", ">=", $today)
            ->orderBy('created_at', 'desc')
            ->limit(2)
            ->get();

        return array("event" => $event, "country" => $country, "timelimit"  => $timelimit);
    }

    /*
        EVENT
    */

    public function getAllEvents($latitude, $longitude) {

        // Logotype for event + id
        $events = Festivity::where('event_type', '!=', 'student')
            ->get()
            ->toArray();

        $all = Distance::sortByDistance($events, $latitude, $longitude);
        $all = array_filter($all, "self::filterInactive");

        $events = Festivity::where('event_type', '!=', 'student')
            ->limit(20)
            ->get()
            ->toArray();

        $limit = Distance::sortByDistance($events, $latitude, $longitude);
        $limit = array_filter($limit, "self::filterInactive");

        return array("all" => $all, "limit" => $limit);
    }

    public function getEventPosts($type, $id, $user, $last)
    {
        $user = parent::returnUser($user);
        if ($last == 0) {
            if ($type == 'timelimit') {
                // Posts for event
                $posts = TimelimitPost::where('owner_event', '=', $id)
                    ->where('isVisible', '=', '1')
                    ->orderBy('created_at', 'desc')
                    ->limit(14)
                    ->get();
            } else {
                // Posts for event
                $posts = Post::where('owner_event', '=', $id)
                    ->where('isVisible', '=', '1')
                    ->orderBy('created_at', 'desc')
                    ->limit(14)
                    ->get();
            }
        } else {
            if ($type == 'timelimit') {
                // Posts for event
                $posts = TimelimitPost::where('id' , '<', $last)
                    ->where('owner_event', '=', $id)
                    ->where('isVisible', '=', '1')
                    ->orderBy('created_at', 'desc')
                    ->limit(3)
                    ->get();
            } else {
                // Posts for event
                $posts = Post::where('id' , '<', $last)
                    ->where('owner_event', '=', $id)
                    ->where('isVisible', '=', '1')
                    ->orderBy('created_at', 'desc')
                    ->limit(3)
                    ->get();
            }
        }
        foreach ($posts as $p) {
            $p['liked'] = false;

            if($type == 'timelimit') {
                $likes = TimelimitLike::where('post', '=', $p["id"])->get();
            } else {
                $likes = Like::where('post', '=', $p["id"])->get();
            }

            if (count($likes) > 1) {
                foreach ($likes as $l) {
                    if($l["owner_user"] == $user->id) {
                        $p['liked'] = true;
                    }
                }
            } else if(count($likes) == 1) {
              if($likes[0]["owner_user"] == $user->id) {
                  $p['liked'] = true;
              }
            }

            $p["likes"] = count($likes);
        }
        return $posts;
    }

    /*
        SCHOOL
    */

    public function getAllSchools($latitude, $longitude) {
        $all = Festivity::where('event_type', '=', 'student')
            ->where('isVisible', '=', '1')
            ->get()
            ->toArray();

        $all = array_filter($all, "self::filterInactive");
        $all = Distance::sortByDistance($all, $latitude, $longitude);

        $limit = Festivity::where('isVisible', '=', '1')
            ->where('event_type', '=', 'student')
            ->limit(20)
            ->get()
            ->toArray();

        $limit = Distance::sortByDistance($limit, $latitude, $longitude);
        $limit = array_filter($limit, "self::filterInactive");


        return array("all" => $all, "limit" => $limit);
    }

     /*
        Country
    */

    public function getAllTimelimits($latitude, $longitude)
    {
        $all = Timelimit::where('deleted_at', '=', null)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        $all = array_filter($all, "self::filterInactiveWatermarks");

        $limit = Timelimit::where('deleted_at', '=', null)
            ->orderBy('created_at', 'desc')
            ->limit(20)
            ->get()
            ->toArray();

        $limit = array_filter($limit, "self::filterInactiveWatermarks");

        return array("all" => $all, "limit" => $limit);
    }


    /*
        Remove inactive events
    */
    private static function filterInactive($e) {
        $posts = Post::where('owner_event', '=', $e["id"])->count();
        if($posts > 0) {
            return $e;
        }

    }

    private static function filterInactiveWatermarks($e) {
        $posts = TimelimitPost::where('owner_event', '=', $e["id"])->count();
        if($posts > 0) {
            return $e;
        }

    }

}
