<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;
    protected $table = 'users';
    protected $fillable = array('name', 'facebook_id', 'email');
    protected $dates = ['deleted_at'];
}
