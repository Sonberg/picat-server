<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimelimitLike extends Model
{
    protected $table = 'timelimit_likes';
    protected $fillable = array('post', 'owner_user');
}
