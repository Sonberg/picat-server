<?php

namespace App;
use Session;

class SessionHandler {
    public static function all() {
        if(\Session::all() !== null) {
            $type = '';
            $msg = '';
            
            $types = ['error', 'success', 'log'];

            foreach($types as $t) {
                if(\Session::get($t) !== null) {
                    $msg = Session::pull($t, 'default');
                    $type = $t;
                }
            }

            return [$type, $msg];
        } else { return null; }
    }
    
}