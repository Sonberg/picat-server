<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CountryPost extends Model
{
    use SoftDeletes;
    protected $table = 'countries_posts';
    protected $fillable = array('isVisible', 'text', 'url');
    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo('App/User');
    }
    
    public function country() {
        return $this->belongsTo('App/Country');
    }

}
