<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Watermark extends Model
{
    use SoftDeletes;
    
    protected $table = 'watermarks';
    protected $fillable = array('url', 'isVisible', 'owner_event', 'owner_user');
    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo('App/User');
    }
    
    public function festivity() {
        return $this->belongsTo('App/Festivity');
    }
}
