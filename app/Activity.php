<?php

namespace App;

use DB;
use DateTime;
use App\Post;
use Carbon\Carbon;

class Activity {

    /*
        ACTIVITY YEAR
    */
    public static function calcYear($id) {
        $startTime = (Int) Carbon::parse()->addYear(-1)->format('m');
        $yearArr = [];

        for($i=0;$i<13;$i++) {
            if($i == 13) {
              $firstDay = (string) Carbon::parse()->startOfMonth()->addYear(0)->addMonth(+1)->format('Y-m-d');

            } else {
              $firstDay = (string) Carbon::parse()->startOfMonth()->addYear(-1)->addMonth($i)->format('Y-m-d');
            }
            $lastDay = Carbon::parse($firstDay)->endOfMonth();

            $start = $firstDay . ' 00:00:00';
            $end = $lastDay . ' 23:59:59';

            $count = Post::whereBetween('created_at', array($start, $end))
                ->where('owner_event', $id)
                ->count();
            $month = Carbon::parse()->addYear(-1)->addMonth($i);
            setlocale(LC_TIME, '');
            $yearArr[$month->formatLocalized('%B %Y')] = $count;
        }
        return $yearArr;
    }

    /*
        ACTIVITY MONTH
    */
    public static function calcMonth($id) {
        $today = explode("-", date("y-m-d"), 3);
        $startTime = Carbon::parse($today[0] . '-'. ($today[1] - 1) .'-'. ($today[2]))->addDay(1);
        $endTime = Carbon::parse($today[0] . '-'. $today[1] .'-'. ($today[2]));
        $interval = $startTime->diff($endTime);
        $daysMonth = intval($interval->format('%R%a') + 1);
        $monthArr = [];

        for($i=0;$i<$daysMonth;$i++) {
            if($i == $daysMonth) {
              $day = Carbon::parse()->addMonth(0)->addDay(0)->format('Y-m-d');
            } else {
              $day = Carbon::parse()->addMonth(-1)->addDay($i)->format('Y-m-d');
            }
            $start = $day . ' 00:00:00';
            $end = $day . ' 23:59:59';
            $count = Post::whereBetween('created_at', array($start, $end))
                ->where('owner_event', $id)
                ->count();
            $date = Carbon::parse()->addMonth(-1)->addDay($i);
            setlocale(LC_TIME, '');
            $monthArr[$date->formatLocalized('%A %d %B')] = $count;
        }
        return($monthArr);
    }

    /*
        ACTIVITY WEEK
    */
    public static function calcWeek($id) {
        $today = explode("-", date("y-m-d"), 3);
        $startTime = Carbon::parse()->addDay(-7);
        $endTime = new DateTime($today[0] . '-'. $today[1] .'-'. $today[2]);
        $interval = $startTime->diff($endTime);
        $daysWeek = ((Int) $interval->format('%R%a') + 1);
        $weekArr = [];

        for($i=0;$i<8;$i++) {
            if($i == 8) {
              $day = Carbon::parse()->addWeek(0)->addDay(0)->format('Y-m-d');
            } else {
              $day = Carbon::parse()->addWeek(-1)->addDay($i)->format('Y-m-d');
            }
            $start = $day . ' 00:00:00';
            $end = $day . ' 23:59:59';

            $count = Post::whereBetween('created_at', array($start, $end))
                ->where('owner_event', $id)
                ->count();
            $day = Carbon::parse()->addWeek(-1)->addDay($i);
            setlocale(LC_TIME, '');
            $weekArr[$day->formatLocalized('%A %d')] = $count;
        }

        return $weekArr;
    }

}
