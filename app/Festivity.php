<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Festivity extends Model
{
    use SoftDeletes;
    protected $table = 'events';
    protected $fillable = array('isVisible','owner_company' ,'title', 'thumb', 'url', 'description', 'latitude', 'longitude', 'radius', 'event_type','date_start', 'date_stop');

    protected $dates = ['deleted_at'];

    public function post() {
        return $this->hasMany('App/Post');
    }

    public function user() {
        return $this->belongsTo('App/User');
    }

}
