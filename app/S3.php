<?php

namespace App;

use App;
use Aws\S3\S3Client;
use App\SessionHandler;

class S3 {
    public static function put($image, $type) {
        $s3 = App::make('aws')->createClient('s3');
        $extension = $image->getClientOriginalExtension();

        if (strlen($extension) > 1 ) {
            $fileName = rand(11111, 99999) . '.' . $extension;
        } else {
            $fileName = rand(11111, 99999) . '.jpg';
        }

        try {
            $s3->putObject(array(
                'Bucket'     => 'picat-eu',
                'Key'        => $type . '/' . $fileName,
                'SourceFile' => $image,
                'ACL'         => 'public-read',
            ));
            return "https://picat-eu.s3-eu-west-1.amazonaws.com/" . $type . "/" . $fileName;

        } catch (Aws\Exception\S3Exception $e) {
            session(["error" => "There was an error uploading the file.\n"]);
            return false;
        }
    }

    public static function delete($fileName, $type) {
        $s3 = App::make('aws')->createClient('s3');
        $s3->deleteObject(array(
            'Bucket' => 'picat-eu',
            'Key'    => $type . '/' . $fileName
        ));

        return true;
    }

}
