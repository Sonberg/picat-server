<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimelimitPost extends Model
{
    use SoftDeletes;
    protected $table = 'timelimit_posts';
    protected $fillable = array('isVisible', 'text', 'url');
    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo('App/User');
    }
    
    public function timelimit() {
        return $this->belongsTo('App/Timelimit');
    }

}
