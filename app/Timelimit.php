<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timelimit extends Model
{
    use SoftDeletes;
    protected $table = 'timelimit';
    protected $fillable = array('title', 'thumb', 'url', 'date_start', 'date_stop');
    protected $dates = ['deleted_at'];
}
