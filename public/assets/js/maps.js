var lat = $("#lat").val();
var lng = $("#lng").val();
var radius = parseInt($("#radius").val()) || parseInt($("output#rangevalue").html());
console.log(radius);
$("input[type='range']").on('input', function () {
    radius = parseInt($("output#rangevalue").html());

    var o = {radius: radius};
    for (var i = 0; i < circles.length; i++) {
        circles[i].setOptions(o);
    }
})

var map,
    position,
    markers = [],
    circles = [];

function initMap() {
    if (lat.length > 1) {
        var pos = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        }
        showPosition(pos);
        addMarker(pos);

    } else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        showPosition({
            lat: 11,
            lng: 56
        });
    }
}


function showPosition(pos) {
    position = function (pos) {
        if (pos.lat !== undefined) {
            return pos;
        } else {
            console.log(pos);
            return {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
            };
        }
    }

    map = new google.maps.Map(document.getElementById('map'), {
        center: position(pos),
        scrollwheel: false,
        zoom: 18
    });

    map.addListener('click', function (event) {
        $("#lat").attr("value", event.latLng.lat);
        $("#lng").attr("value", event.latLng.lng);
        $("#radius").attr("value", radius);

        addMarker(event.latLng);
    });
}

function addMarker(location) {
    setMapOnAll(null);
    markers = [];
    circle = {};

    console.log(radius);

    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    var circle = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: location,
        radius: radius
    });

    markers.push(marker);
    circles.push(circle);
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
    for (var i = 0; i < circles.length; i++) {
        circles[i].setMap(map);
    }
}
