$(document).ready(function () {
    /*
        FIX NAV
    */

    var navOffset = ($('.edit-bar').offset().top - $('.top-bar').outerHeight() + 2);

    $(window).on('scroll', function () {
        var scrollPos = $(window).scrollTop();
        $('.bar-placholder').css({
            height: $('.edit-bar').outerHeight()
        });
        if (scrollPos >= navOffset) {
            $('.edit-bar').addClass('fixed');
            $('.edit-bar').css({
                top: $('.top-bar').outerHeight()
            })
        } else {
            $('.edit-bar').removeClass('fixed');
        }
    })

    /*
        CHECKBOX
    */
    $(document).on('change', '.checkbox-post', function () {
        var parent = $(this).siblings();
        if ($(this).prop('checked')) {
            parent.addClass('selected');
        } else {
            parent.removeClass('selected');
        }
        checkItems();
    })

    function checkItems() {
        var items = $('input:checkbox:checked');
        var itemsLength = $('input:checkbox:checked').length;
        $('#num-selected').html(itemsLength);


    }

    var boxes = $('.active-check');
    for (var i = 0; i < boxes.length; i++) {
        if ($(boxes[i]).attr('state') === '1') {
            console.log('checked');
            $(boxes[i]).children('input').prop('checked', true);
        }
    }
    
    
    // Quick Settings
    if($('#event-state').attr('state') === '1') {
        $("#eventVisible").prop('checked', true)
    } else {
        $("#eventHidden").prop('checked', true)   
    }
    
    $('#event-state').on('change', function(e) {
        $(this).closest('form').get(0).submit()
    })
    
    if($('#postsIsVisible').attr('state') === '1') {
        $("#newPostStatusVisible").prop('checked', true)
    } else {
        $("#newPostStatusHidden").prop('checked', true)   
    }
    
    $('#postsIsVisible').on('change', function(e) {
        $(this).closest('form').get(0).submit()
    })
    
    
})