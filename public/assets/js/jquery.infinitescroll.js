var infinitScroll = function (options) {
    var event = $('.scroll').attr('event');
    var count = 1;
    var didGetData = true;

    $(document).ready(function() {
        loadMore();
        count++;
    })

    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10 && didGetData) {
            loadMore();
            count++;
        }
    }).scroll();

    function loadMore() {
        var url = "/dashboard/posts/" + event + "?page=" + count
        if (didGetData) {
            $('#load').show();
            $.ajax({
                type: "GET",
                url: url,
                success: function (response) {
                    if (response) {
                        didGetData = true;
                        $('.scroll').append(response);
                    } else {
                        didGetData = false;
                    }
                    $('#load').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                    console.log(textStatus);
                }
            });
        }
    }
}
