"use strict";

$('.clear-upload').on('click', function () {
    $(".wm-upload").replaceWith($(".wm-upload").val('').clone(true));
    document.getElementById('list').innerHTML = "";
});


function handleFileSelect(evt) {
    $('#list').html("");
    var files = evt.target.files;

    if (files.length <= 2) {
        $('.clear-upload').show();
        // Loop through the FileList and render image files as thumbnails.
        for (var i=0; i<files.length; i++) {
            var f = files[i];
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = [
            '<img style="height: 175px; margin: 5px" src="',
            e.target.result,
            '" title="', escape(theFile.name),
            '"/>'
          ].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    } else {
        alertify.error('Too many files');
        reset($('.wm-upload'));
    }
}

window.reset = function (e) {
    $('.clear-upload').hide();
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

$('.wm-upload').on('change', handleFileSelect);

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumb-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('#logotype').change(function () {
    readURL(this);
});